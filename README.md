#Welcome one and all!
Welcome to the LAN ip scanner. This scanner pings every ip on the current network. This allows you to see a list of devices that are connected!

##Why?
Though my very limited networking experince, I have decided to make a program that is able to scan a LAN connection for IP's. The original purpose was to see if there were any devices on the network that are not sappose to be there.

##Goal
My goal is to make a full GUI and runnable program (using java or something) in order to run the scan. I am thinking of having 2 seperate modes:

* Monitor
    * Scan the network continuously and warn the user if there is a new device connected, if one disconnects, and other information
    * gives the user access to monitor connections
* Single Scan
    * preforms a single scan to locate devices on the network. This can be used to find network devices such as printers, computers, servers etc..
    
##How to Use:
###Startup
Open a terminal window, "cd" into the directory where the IPScanner.sh file is located
type the following command then hit "enter / return":
sudo sh IPScanner.sh
###Using the program
Wait for the program to finish running its file checks (deleting and creating the text files required).
* When asked for the delay value:
    * 0.1 recommend for quick scan (not as accurate)
    * 0.2 recommended default
    * 0.5 for detailed scan (takes much longer to finish)
* when asked for either running a 1 time exe or monitor
    * 1 - one time execution
    * 0 - continuous monitor (will send a --restart-- to the log file)

##Updates:
###V 0.3 (current):
Since I have been working slowly on this before I actually made the repository, this is version 3. The program runs like so:

1. Scan network for xxx.xxx.xxx.1 (modem) and ask [y/N] to open for login
    * if yes, it will take you to the site
    * if no, it will exit and contiue the program
2. Program will then grab IP, print network information (including network driver IP) and set the first 3 sets of numbers on the ip to a variable.
    * when scanning, the first 3 sets will be taken and the last digit will be looped though to scan from 0 - 255 eg: ip = 192.168.0.3, program will scan: 192.168.0.{0-255}
3. Program will then ask for delay
    * the quicker the delay (eg 0.2) the less accurate it will be
    * the longer the delay (0.5), the longer the scan will take
4. Program will ask for either monitor mode, or single scan
    * Monitor mode will continuosuly scan IP's on the network
    * single scan will scan a single time
5. Program will begin scan of IP's
    * current animation:
        * I currently have a counting animation that will let you know at what IP you are on
    * scans IP's on network from {0-255}
    * can take a couple of minuites
6. Program will print results of IP scan with included timestamp, IP address and number of IP's found.
7. Program exits

##Current Bugs / Errors / Limitations
* Only runs on Mac OSX Terminal (mac bash only)
* only works for full IP's (eg. 192.168.000.1) not shortend IP's (eg. 192.168.0.1)
* Most likely spelling mistakes (im not an english major mom!)

##One last thing...

Have fun! I love programming and I love to share what I learned! I create code becuase I like to and I love being able to collorabte with other people who know much more than me!