#---------------------------------------------
#   Made By Glenn Cottam
#
#   Opensource Program!
#   Visit: https://bitbucket.org/GlennCottam/local-ip-scanner
#   For repository
#   
#   I am not responsable if you get into any
#   trouble using this code...
#
#   Enjoy the program!
#---------------------------------------------
#!/bin/bash
clear
echo ""
echo "_________     _________"
echo "   |          |       |"
echo "   |          |       |"
echo "   |          |_______|"
echo "   |          |"
echo "   |          |"
echo "_________     |"
echo "      SCANNER"
sleep 0.5
echo ""
#read -p "Enter time delay for program:     " delay
delay=0.1
echo "SCANNER ACTIVATED"
sleep $delay
rm dump.txt
touch dump.txt
rm log.txt
touch log.txt
touch Found.txt 
rm Found.txt
sleep $delay
echo "Cache cleared"
echo "IP information:" >> log.txt
echo "______________________________" >> log.txt
echo "xX_Wireless_Xx" >> log.txt
ifconfig -u en1 >> log.txt
echo "" >> log.txt
echo "xX_Ethernet_Xx" >> log.txt
ifconfig -u en0 >> log.txt
echo "" >>log.txt
echo "Your IP Address" >>log.txt
en0=$(ipconfig getifaddr en0)
en1=$(ipconfig getifaddr en1)
ip=${en1::+11}
echo "Scanning for modem ($ip.1)"
read -r -p "Go to gateway portal? [y/n]:	" gateway
if [[ "$gateway" =~ ^([yY][eE][sS]|[yY])+$ ]]
	then
		open http://$ip.1
		read -n1 -r -p "Press any key to continue" key
	else
		echo "Canceled Opening Gateway Portal"
fi
sleep 1
echo "EN0:  " $en0 >> log.txt
echo "EN1:  " $en1 >> log.txt
echo "______________________________" >> log.txt
echo ""
echo "Printing log..."
sleep $delay
echo "------LOG START------"
sleep $delay
echo ""
cat log.txt
sleep $delay
echo ""
echo "-------LOG END-------"
echo ""
echo "Time delay (0.1 for quick scan, 0.5 fir detailed scan, 0.2 recomended)"
read -p "Enter time delay:    " len
echo "0 = monitor, 1 = one time exe"
read -p "Enter 1/0 value:   " value
if [[ value -eq 1 ]];
    then
        (( num = 255 ))
    else
        (( num = 256 ))
        echo "To exit, press CTRL + Z"
        sleep 1
fi
echo "$ip will be scanned"
read -n1 -r -p "Press Any Key to Continue" key
if [ "$key" = '' ];
    then
    time=$(date "+%Y.%m.%d-%H.%M.%S")
    sleep 0.5
#SYSTEM START
    ((i = 0))
    ((click = 0))
    ((ani = 1))
    while [[ $i -lt $num ]] ; do
        if [[ $i -eq 255 ]] ; 
            then
                (( i = 0 ))
                echo "__________Restart__________" >> log.txt
                cho "__________Restart__________" >> Found.txt
                cat log.txt
                cat Found.txt
                echo "Number of devices Found: $click" >> log.txt
                echo "Number of devices Found: $click"
                printf "Restarting in 5"
                sleep 0.5
                printf "."
                sleep 0.5
                printf "."
                sleep 0.5
                printf "4"
                sleep 0.5
                printf "."
                sleep 0.5
                printf "."
                sleep 0.5
                printf "3"
                sleep 0.5
                printf "."
                sleep 0.5
                printf "."
                sleep 0.5
                printf "2"
                sleep 0.5
                printf "."
                sleep 0.5
                printf "."
                sleep 0.5
                printf "1"
                sleep 0.5
                printf "."
                sleep 0.5
                printf "."
                sleep 0.5
                printf "0"
                sleep 0.5
                printf "."
                sleep 0.5
                printf "."
                sleep 0.5
                ((click = 0))
        fi
        ((i = i + 1))
        ((count = 1))
        while [[ $count -ne 0 ]] ; 
            do
                ping -s 1 -i $len -W $len -c 1 $ip.$i >> dump.txt
                rc=$?
                if [[ $rc -eq 0 ]] ; 
                    then
                        ((count = 1))
                fi
		if [[ ani -ne 9 ]];
			then
				((ani = ani +1))
		fi
		if [[ ani -eq 9 ]];
			then
				((ani = 1))
		fi
            ((count = count - 1))
        done
        if [[ $rc -eq 0 ]] ; 
            then
                current_time=$(date "+%Y.%m.%d-%H.%M.%S")
                echo "[$current_time]:      $ip.$i" >> Found.txt
                clear
                echo "Scanned: $ip.$i"
                echo "$i / 255"
		cat Found.txt
                ((click = click + 1))
            else
                clear
                echo "Scanned: $ip.$i"
                echo "$i / 255"
                cat Found.txt
        fi
    done
fi
echo ""
echo "EXECUTION COMPLETE"
cat Found.txt >> log.txt
sleep 1
clear
echo "Results:"
sleep $delay
echo "------LOG START------"
sleep $delay
echo ""
echo "Number of devices Found: $click" >> log.txt
cat log.txt
sleep $delay
echo ""
echo "-------LOG END-------"
sleep $delay
echo ""
echo "Scan Complete"
echo "TERMINATING PROGRAM"
pkill -f IPScanner\(v3\).sh
